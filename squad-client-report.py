import sys
from string import Template
import pathlib
import tabulate as tb
import markdown2 as md
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad
import fetcher


def html_template(html_body_str, template_file='template.html'):
    """ Generates a full html file string for a given html body """
    with open(template_file, 'r') as tmpl_file:
        template = Template(tmpl_file.read())
    return template.substitute(html_body=html_body_str)


def incomplete_project_jobs(squad_url, group_name, project_name, output_dir):
    """ Generate an overview of incomplete jobs for a given SQUAD project """

    SquadApi.configure(url=squad_url)
    group = Squad().group(group_name)
    project = group.project(project_name)

    gen = fetcher.fetch_incomplete_jobs(project)
    header = next(gen)
    markdown_str = f'# Incomplete jobs in `{project_name}`\n' \
                 + tb.tabulate(list(gen), header, tablefmt='github')
    html_str = html_template(md.markdown(markdown_str, extras=['tables']))

    with open(output_dir / 'incomplete_jobs.html', 'w') as html_file:
        html_file.write(html_str)


if __name__ == '__main__':
    out = pathlib.Path().cwd()
    if len(sys.argv) > 1:
        out = pathlib.Path(sys.argv[1])
    incomplete_project_jobs(
        squad_url='https://qa-reports.linaro.org/',
        group_name='lkft',
        project_name='linux-stable-rc-linux-6.6.y',
        output_dir=out
    )
